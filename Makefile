################################################################
# Makefile for MathSeer Pipeline
#
# R. Zanibbi, Aug. 12, 2021
#################################################################

# Weight file names (will change)
SSDW="ssd512GTDB_256_epoch15.pth"
SSDW_C="ssd512GTDB_256_epoch7_chem.pth"
# QDDW="net_39.pt"
QDDW="net_89.pt"

##################################
# Installation
###################################
# default: build all, stop on failure
all:
	./bin/install

# 'make force' - do not stop on failure
force:
	./bin/install force

##################################
# Example executions
###################################
# 'make example' - run small ACL collection
example:
	./run-msp --input_dir ./inputs/ACL --output_dir ./outputs/ACL \
		--convert_pdfs 1  -v 1 -pm 1

example-yolo:
	./run-msp --input_dir ./inputs/ACL --output_dir ./outputs/ACL \
		--convert_pdfs 1  -v 1 --detector yolo

# 'make sacl' - run just one page from each ACL file.
sacl:
	./run-msp --input_dir ./inputs/ACL-small --output_dir ./outputs/ACL-small \
		--convert_pdfs 1 -v 1

sacl-yolo:
	./run-msp --input_dir ./inputs/ACL-small --output_dir ./outputs/ACL-small \
		--convert_pdfs 1 -v 1 --detector yolo


# 'make acl2' - run two pages from 2 ACL docs
acl2:
	./run-msp --input_dir ./inputs/ACL-2 --output_dir ./outputs/ACL-2 \
		--convert_pdfs 1 -v 1

acl2-yolo:
	./run-msp --input_dir ./inputs/ACL-2 --output_dir ./outputs/ACL-2 \
		--convert_pdfs 1 -v 1 --detector yolo

# 'make k15' - run single ACL doc
k15:
	./run-msp --input_dir ./inputs/K15-1002 --output_dir ./outputs/K15-1002 \
		--convert_pdfs 1 -v 1

k15-yolo:
	./run-msp --input_dir ./inputs/K15-1002 --output_dir ./outputs/K15-1002 \
		--convert_pdfs 1 -v 1 --detector yolo

# 'make 1-example' - run one complete ACL file.
1-example:
	./run-msp --input_dir ./inputs/A00_3007 --output_dir ./outputs/A00_3007 \
		--convert_pdfs 1 -v 1

1-example-yolo:
	./run-msp --input_dir ./inputs/A00_3007 --output_dir ./outputs/A00_3007 \
		--convert_pdfs 1 -v 1 --detector yolo

# 'make qexample' - run small ACL collection, don't regenerate images or vis.
qexample:
	./run-msp --input_dir ./inputs/ACL --output_dir ./outputs/ACL 

qexample-yolo:
	./run-msp --input_dir ./inputs/ACL --output_dir ./outputs/ACL --detector yolo

# 'make chem-test' - run 1 Chem CLEF document
chem-test:
	./run-msp --input_dir ./inputs/CLEF_TEST --output_dir ./outputs/CLEF_TEST --mode 1 \
	    --convert_pdfs 1 -v 1

chem-test-yolo:
	./run-msp --input_dir ./inputs/CLEF_TEST --output_dir ./outputs/CLEF_TEST --mode 1 \
	    --convert_pdfs 1 -v 1 --detector yolo


# 'make chem-test-multi' - run 2 Chem CLEF documents
chem-test-2:
	./run-msp --input_dir ./inputs/CLEF_TEST_2 --output_dir ./outputs/CLEF_TEST_2 --mode 1 \
	    --convert_pdfs 1 -v 1

chem-test-2-yolo:
	./run-msp --input_dir ./inputs/CLEF_TEST_2 --output_dir ./outputs/CLEF_TEST_2 --mode 1 \
	    --convert_pdfs 1 -v 1 --detector yolo

##################################
# Cleaning and Reinstallation
###################################
# 'make clean-out' remove only output data from running examples
clean-out:
	rm -fr ./outputs/*

# 'make clean-example' - remove all data from running examples
clean-example:
	rm -fr ./outputs/*
	rm -fr ./inputs/*/images/*

# 'make clean' - delete all items from installation script and example.
clean: clean-example
	rm -fr ./modules/lgeval
	rm -f  ./modules/scanssd/ssd/trained_weights/${SSDW}
	rm -f  ./modules/scanssd/ssd/trained_weights/${SSDW_C}
	rm -f  ./modules/qdgga-parser/outputs/run/infty_contour/${QDDW}
	rm -f  ./run-msp

# 'make clean-repos' - remove repositories in the modules directory.
clean-repos:
	rm -fr ./modules/symbolscraper-server
	rm -fr ./modules/scanssd
	rm -fr ./modules/qdgga-parser
	rm -fr ./modules/protables
	rm -rf ./modules/scaled-yolov4


# 'make clean-full' - remove all installation data.
clean-full: clean clean-repos

# 'make conda-remove' - remove mathseer conda environment
conda-remove:
	conda env remove -n mathseer

# The 'start again' rule.
# 'make rebuild' - removes conda environment, cleans everything,  reinstalls pipeline
rebuild: conda-remove clean-full all


##################################
# Remove logs
###################################
# 'make clean-logs' removes all .log files in the logs folder and its sub-folders
clean-logs:
	rm -f ./logs/*.log
	rm -f ./logs/aiopipeline/*.log

download-ACL-10:
	./bin/download-acl-subset ACL-subsets/acl10.txt ACL-10

download-ACL-25:
	./bin/download-acl-subset ACL-subsets/acl25.txt ACL-25

download-ACL-50:
	./bin/download-acl-subset ACL-subsets/acl50.txt ACL-50

download-ACL-100:
	./bin/download-acl-subset ACL-subsets/acl100.txt ACL-100

download-ACL-500:
	./bin/download-acl-subset ACL-subsets/acl500.txt ACL-500

download-ACL-1000:
	./bin/download-acl-subset ACL-subsets/acl1k.txt ACL-1000


download-ACL-year-1952:
	./bin/download-acl-subset ACL-subsets/year_chunks/1952.txt ACL-year-1952

download-ACL-year-1956:
	./bin/download-acl-subset ACL-subsets/year_chunks/1956.txt ACL-year-1956

download-ACL-year-1957:
	./bin/download-acl-subset ACL-subsets/year_chunks/1957.txt ACL-year-1957

download-ACL-year-1960:
	./bin/download-acl-subset ACL-subsets/year_chunks/1960.txt ACL-year-1960

download-ACL-year-1996:
	./bin/download-acl-subset ACL-subsets/year_chunks/1996.txt ACL-year-1996

download-ACL-year-2020:
	./bin/download-acl-subset ACL-subsets/year_chunks/2020.txt ACL-year-2020

download-ACL-year-2021:
	./bin/download-acl-subset ACL-subsets/year_chunks/2021.txt ACL-year-2021

download-ACL-year-2022:
	./bin/download-acl-subset ACL-subsets/year_chunks/2022.txt ACL-year-2022

download-ACL-year-2023:
	./bin/download-acl-subset ACL-subsets/year_chunks/2023.txt ACL-year-2023

ACL-10: download-ACL-10
	./run-msp --input_dir ./inputs/ACL-10 --output_dir ./outputs/ACL-10 \
		--convert_pdfs 0  -v 1

ACL-10-yolo: download-ACL-10
	./run-msp --input_dir ./inputs/ACL-10 --output_dir ./outputs/ACL-10 \
		--convert_pdfs 1  -v 0 --detector yolo

ACL-25: download-ACL-25
	./run-msp --input_dir ./inputs/ACL-25 --output_dir ./outputs/ACL-25 \
		--convert_pdfs 1  -v 0

ACL-25-yolo: download-ACL-25
	./run-msp --input_dir ./inputs/ACL-25 --output_dir ./outputs/ACL-25 \
		--convert_pdfs 1  -v 0 --detector yolo

ACL-50: download-ACL-50
	./run-msp --input_dir ./inputs/ACL-50 --output_dir ./outputs/ACL-50 \
		--convert_pdfs 1  -v 0

ACL-50-yolo: download-ACL-50
	./run-msp --input_dir ./inputs/ACL-50 --output_dir ./outputs/ACL-50 \
		--convert_pdfs 1  -v 0 --detector yolo

ACL-100: download-ACL-100
	./run-msp --input_dir ./inputs/ACL-50 --output_dir ./outputs/ACL-50 \
		--convert_pdfs 1  -v 1

ACL-year-1952: download-ACL-year-1952
	./run-msp --input_dir ./inputs/ACL-year-1952 --output_dir ./outputs/ACL-year-1952 \
                --convert_pdfs 1  -v 0

ACL-year-1956: download-ACL-year-1956
	./run-msp --input_dir ./inputs/ACL-year-1956 --output_dir ./outputs/ACL-year-1956 \
                --convert_pdfs 1  -v 0

ACL-year-1957: download-ACL-year-1957
	./run-msp --input_dir ./inputs/ACL-year-1957 --output_dir ./outputs/ACL-year-1957 \
                --convert_pdfs 1  -v 0

ACL-year-1960: download-ACL-year-1960
	./run-msp --input_dir ./inputs/ACL-year-1960 --output_dir ./outputs/ACL-year-1960 \
                --convert_pdfs 1  -v 0

ACL-year-1996: download-ACL-year-1996
	./run-msp --input_dir ./inputs/ACL-year-1996 --output_dir ./outputs/ACL-year-1996 \
		--convert_pdfs 1  -v 0

ACL-year-2020: download-ACL-year-2020
	./run-msp --input_dir ./inputs/ACL-year-2020 --output_dir ./outputs/ACL-year-2020 \
		--convert_pdfs 1  -v 0

ACL-year-2021: download-ACL-year-2021
	./run-msp --input_dir ./inputs/ACL-year-2021 --output_dir ./outputs/ACL-year-2021 \
		--convert_pdfs 1  -v 0

ACL-year-2022: download-ACL-year-2022
	./run-msp --input_dir ./inputs/ACL-year-2022 --output_dir ./outputs/ACL-year-2022 \
		--convert_pdfs 1  -v 0

ACL-year-2023: download-ACL-year-2023
	./run-msp --input_dir ./inputs/ACL-year-2023 --output_dir ./outputs/ACL-year-2023 \
		--convert_pdfs 1  -v 0

ACL-100-yolo: download-ACL-100
	./run-msp --input_dir ./inputs/ACL-50 --output_dir ./outputs/ACL-50 \
		--convert_pdfs 1  -v 1 --detector yolo


