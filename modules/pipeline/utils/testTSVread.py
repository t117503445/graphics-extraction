##################################
# testTSVread.py
##################################

import sys
from src.utils.msFileIO import *

# Usage: pass a TSV to read.
if __name__ == "__main__":
    ( docList, docs_page_sizes ) = read_pro_tsv( sys.argv[1] )

    for entry in docList:
        pro_summary( entry[1], sys.argv[1] )

    # Write to current directory, using an empty symbol map.
    write_pro_tsv( os.getcwd(), docList, {} )

