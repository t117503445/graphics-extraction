from typing import List

from pdf2image import convert_from_path
import sys
import logging
import os
from glob import glob
from multiprocessing import Pool
import time
import gc
import cv2
import numpy as np
from PIL.Image import Image
from utils.img2LG import extract_ccs, write_LG
from modules.protables.ProTables import *

from PIL.Image import Image
from tqdm import tqdm

"""
    This module converts directory containing PDF files to
    PNG images (one per page), saving them in an output directory
    Usage: python convert_pdf_to_image.py <PDF_files_dir> <output_img_dir>

    Additional routines for image processing included.
"""
# pdf_dir = ''
# output_dir = ""

# Set page image render size (used with ScanSSD + QD-GGA)
DPI = 256 

def show_image( img, title="" ):
    print(">> Showing image: " + title)
    cv2.imshow( 'image', img)
    cv2.waitKey(0)

################################################################
# PDF TO PNG Conversion
################################################################
def pdf_to_png( triple ):
    # Filenames are without paths (e.g., a.pdf, not a/b/c.pdf)
    ( pdf_dir, output_dir, pdf_files ) = triple

    # Each file will have individual page images written to disk.
    for pdf_file in pdf_files: 
        pdf_name = pdf_file.split(".pdf")[0]

        # Set up output path for document.
        output_path = os.path.join( output_dir, pdf_name )
        if not os.path.exists( output_path ):
            os.makedirs(output_path)

        # Read PDF, write 1 PNG per page to output directory for that PDF doc
        pages = convert_from_path( os.path.join(pdf_dir, pdf_file), DPI )
        for i in range(len(pages)):
            pages[i].save( os.path.join(output_path,  str(i + 1) + ".png"), "PNG")

        # Clean up.
        del pages
        gc.collect()


def create_images_from_pdfs( pdf_dir,  output_dir ):
    """
    Render each PDF file as image 
    :param pdf_dir: Directory with PDF files
    :param output_dir: Output directory for storing image files for each PDF file
    :return: None
    """
    logging.debug("  PDF input dir: " + pdf_dir )
    logging.debug("  PNG output dir: " + output_dir )
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Collect PDF file list.
    logging.debug(os.path.join(pdf_dir, '*.pdf'))
    pdf_files = glob(os.path.join(pdf_dir, '*.pdf'))
    pdf_files = [pdf.split('/')[-1] for pdf in pdf_files]
    # for (_, _, fileList) in os.walk(pdf_dir):
    #     pdf_files.extend(fileList)
    #     break
    logging.debug("  " + str(len(pdf_files)) + " PDF file(s) to convert")

    # Partition files evenly among processes (# cores)
    # If very few files, then create each chunk with only one file
    # If fewer files than cores, create one process per file.
    n_processes = os.cpu_count()
    n = int(len(pdf_files) / n_processes)

    if n < 1.0:
        n = 1
        n_processes = len(pdf_files)

    chunks = [ pdf_files[x:x+n] for x in range(0, len(pdf_files), n) ]
    num_chunks = len(chunks)
    infiles = [ pdf_dir ] * num_chunks
    outfiles  = [ output_dir ] * num_chunks
    chunkTuples = list(zip( infiles, outfiles, chunks ) )

    # Create a process pool and execute.
    pool = Pool( processes = n_processes )
    pool.imap( pdf_to_png, chunkTuples )
    pool.close()
    pool.join()

def convert_pdfs(input_dir):
    start = time.time()
    image_path = os.path.join(input_dir, "images")
    pdf_path = os.path.join(input_dir, "pdf")
    if not os.path.isdir(image_path):
        os.makedirs(image_path)
    logging.debug("\n[ Converting PDF pages to PNG images ]")
    
    create_images_from_pdfs( pdf_path, image_path )
    logging.debug("  Conversion time: {:.2f}".format( time.time() - start )  + " seconds")

################################################################
# Raster image utilities
################################################################

def resize(img, scale_percent):
    # scale_percent: percent of original size
    width = int(img.shape[1] * scale_percent)
    height = int(img.shape[0] * scale_percent)
    dim = (width, height)
    # resize image 
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    return resized

'''
    Extract connected components for PNG page images in a directory,
    belonging to one document ('filename')

    Returns a page/region/object table.
'''
def get_cc_objects( input_dir, filename, pageRegionsTable, regionTypeStr ):

    #logging.debug("  * Extracting connected components")

    num_pages = len(pageRegionsTable)
    outROTable = [ [] for page in pageRegionsTable ]

    cc_count = 0
    for p in range( num_pages ):
        # RZ: new function to create interval tree for region in page ( Y dir )
        yT = region_int( pageRegionsTable[p] )

        # Initialize regions
        for r in range( len(pageRegionsTable[p] ) ):
            [ rminX, rminY, rmaxX, rmaxY ] = \
                [ round(c) for c in pageRegionsTable[p][r] ]
            outROTable[p].append( ( (rminX, rminY, rmaxX, rmaxY), [], regionTypeStr ) )

        p_img = cv2.imread(os.path.join(input_dir, "images", filename, \
                str(p+1) + ".png"))
        inv_img = cv2.bitwise_not( p_img )
        cc_boxes = extract_ccs( inv_img, True )

        for bb in cc_boxes:
            (minX, minY, maxX, maxY) = list(map(round, bb))
            yMatches = yT[minY : maxY + 1 ]
            for (begin, end, xInterval) in yMatches:
                (beginX, endX, r) = xInterval
                # No threshold to start; any overlap leads to inclusion
                if xInterval.overlap_size(minX, maxX + 1) > 0:
                   outROTable[p][r][1].append( (minX, minY, maxX, maxY, '_', 'c' ) )

        # Construct the p/r/o table region for each region, populate with
        # CCs in each region (within the current page).
#        get_cc_objects_from_img(outROTable=outROTable,
#                                img=p_img,
#                                page=p,
#                                cc_count=cc_count,
#                                pageRegionsTable=pageRegionsTable,
#                                regionTypeStr=regionTypeStr)

    return outROTable

def get_cc_objects_from_path_list( img_list: List[str], pageRegionsTable, regionTypeStr ):

    #logging.debug("  * Extracting connected components")

    num_pages = len(pageRegionsTable)
    outROTable = [ [] for page in pageRegionsTable ]
    cc_count = 0
    for p, p_img_path in zip(range(num_pages), img_list):

        p_img = cv2.imread(p_img_path)

        # Construct the p/r/o table region for each region, populate with
        # CCs in each region (within the current page).
        get_cc_objects_from_img(outROTable=outROTable,
                                img=p_img,
                                page=p,
                                cc_count=cc_count,
                                pageRegionsTable=pageRegionsTable,
                                regionTypeStr=regionTypeStr)

    return outROTable



def get_cc_objects_from_img_list( img_list: List[Image], pageRegionsTable, regionTypeStr):

    num_pages = len(pageRegionsTable)
    outROTable = [ [] for page in pageRegionsTable ]
    cc_count = 0
    for p, p_img_pil in zip(range(num_pages), img_list):
        p_img = pil_img_to_cv2_img(p_img_pil)
        get_cc_objects_from_img(outROTable=outROTable,
                                img=p_img,
                                page=p,
                                cc_count=cc_count,
                                pageRegionsTable=pageRegionsTable,
                                regionTypeStr=regionTypeStr)
    return outROTable


def get_cc_objects_from_img(outROTable, img, page, cc_count, pageRegionsTable, regionTypeStr):
    for r in range( len(pageRegionsTable[page] )):
            [ rminX, rminY, rmaxX, rmaxY ] = [ round(c) \
                    for c in pageRegionsTable[page][r]]

            outROTable[page].append( ((rminX, rminY, rmaxX, rmaxY), [], regionTypeStr ) )
            #print( outROTable[p][-1] )

            # Clip region, and then invert B/W.
            clip_img = img[ rminY:rmaxY , rminX:rmaxX  ] #clip_img = img[ rminX:rmaxX, rminY:rmaxY  ]
            inv_img = cv2.bitwise_not( clip_img )

            # Extract list of BBs for connected components
            cc_boxes = extract_ccs( inv_img, True )
            #pcheck( cc_boxes, "cc_boxes")

            cc_count += len( cc_boxes )

            # Need to add top-left corner to get page coordinates.
            for (minX, minY, maxX, maxY ) in cc_boxes:
                outROTable[page][r][1].append( ( rminX + minX, rminY + minY,
                    rminX + maxX, rminY + maxY, '_', 'c' ) )


def pil_img_to_cv2_img(pil_img):
    open_cv_image = np.array(pil_img.convert('RGB'))
    cv2_img = open_cv_image[:, :, ::-1].copy()
    return cv2_img

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python3 convert_pdf_to_image.py <PDF_files_dir> <output_dir>")
        # raise ValueError("Incorrect usage")
        exit(0)

    pdf_dir = sys.argv[1]
    print("  PDF dir (input):  " + pdf_dir)
    output_dir = sys.argv[2]
    print("  PNG dir (output): " + output_dir)

    start = time.time() 
    create_images_from_pdfs( pdf_dir,  output_dir )
    print("  Conversion time: {:.2f} seconds".format( time.time() - start))
