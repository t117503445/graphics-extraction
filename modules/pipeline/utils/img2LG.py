import sys
import cv2
import os.path as osp
import numpy as np
from msp_settings import *

#from utils.msFileIO import pcheck

def extract_ccs(image, norm=False):
    # RZ: Adding parameter to allow standard bounding boxes to be returned
    # in a list.

    # Extract CCs with OpenCV (as 'outer' contours)
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) / 255.0
    ( _, thresh ) = cv2.threshold(image_gray * 255, CC_GRAY_THRESHOLD, 255, 0)
    thresh = np.array(thresh, dtype=np.uint8)
    
    contours, hierarchy = cv2.findContours(
        thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )
    
    n_contours = len(contours)
    boxes = np.zeros((n_contours, 4), dtype="float")
    labels = ["_"] * n_contours

    nboxes = []
    for ( i, cnt ) in enumerate(contours):
        ( x, y, w, h ) = cv2.boundingRect(cnt)
        
        if norm:
            nboxes.append( [x, y, x + w, y + h ] )
        else:
            boxes[i, :] = [y, x, y + h, x + w]

    # Modifying output based on [norm]alization flag.
    if norm:
        return nboxes
    else:
        return ( labels, boxes )


def write_LG(image, lg_outDir, img_outDir, filename, labels=[], boxes=[]):
    """
    Write input LG file with #cc coordinates from input expression image

    Parameters
    ----------
    image : numpy.ndarray
        Input expression image

    Returns
    -------
    None
    """

    #print("WRITING TO: " + lg_outDir )

    prefix = "N, "
    end = ", 1.0"

    # Writing label graph file
    base_file_name = filename + ".lg"
    outfile = open(lg_outDir + "/" + base_file_name, "w")
    outfile.write("# IUD, " + filename + "\n")
    outfile.write("\n# [PRIMITIVE NODES (N)]\n")

    symbols = []
    for i, box in enumerate(boxes):
        name = str(i)
        symbols.append(name)
        label = labels[i]
        outfile.write(prefix + name + ", " + label + end + "\n")

    outfile.write("\n# [PRIMITIVE FEATURES]\n")
    prefix = "#cc, "

    # RZ: Generates a *second* set of labeled input images
    # Write #cc i.e. connected components' box coordinates
    for i, box in enumerate(boxes):
        ( ul_y, ul_x, lr_y, lr_x ) = box
        cv2.rectangle(image, (ul_x, ul_y), (lr_x, lr_y), (0, 255, 0), 1)
        outfile.write(prefix + str(i) + ", " + ", ".join(str(b) for b in box) \
            + "\n")

    # Create expression + symbols images
    cv2.imwrite(osp.join(img_outDir, filename + ".PNG"), image)
    outfile.close()


if __name__ == "__main__":
    image_path = sys.argv[1]
    lg_outDir = sys.argv[2]
    img_outDir = sys.argv[3]
    filename = sys.argv[4]
    image = cv2.imread(image_path)
    write_LG(image, lg_outDir, img_outDir, filename)
