import subprocess
from subprocess import DEVNULL, STDOUT
import os.path as osp
import os
import time
import logging
import requests
from tqdm import tqdm
from modules.pipeline.msp_settings import SSCRAPER_DIR, ROOT_DIR


def process_sscraper(input_dir, output_dir, ):
    start = time.time()
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.debug("\n[ Symbol Scraper ]")
    logging.debug(f"  PDF dir: {input_dir}")
    logging.debug(f"  Output (XML) dir: {output_dir}")
    logging.debug(f"  PDF Documents to process: {len(os.listdir(input_dir))}")

    os.chdir(ROOT_DIR)
    if not osp.isdir(output_dir):
        os.makedirs(output_dir)



    # start symbol scraper service locally (non-docker)
    proc = subprocess.Popen(
        [
            "java",
            "-jar",
            "modules/symbolscraper-server/target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar"
        ],
        stdout=DEVNULL,
        stderr=STDOUT
    )

    time.sleep(3.5)
    pbar = tqdm(total=len(os.listdir(input_dir)), desc="  Processing")
    try:
        for f in os.listdir(input_dir):
            xml_content = sync_call(os.path.join(input_dir, f))
            if xml_content is not None:
                save_to_out_dir(xml_content, output_dir, f)
            pbar.update(1)
    except Exception as e:
        print(e)
        proc.terminate()

    pbar.close()
    proc.terminate()
    end = time.time()
    logging.debug("  Symbol Scraper time: {:.2f}".format(end - start) \
                  + " seconds")


def sync_call(pdf_file: str):
    filename = os.path.basename(pdf_file)

    r = "http://localhost:7002/process-pdf?writePageBorderBBOX=true"

    file = {"pdf": (filename, open(pdf_file, 'rb'), "application/pdf")}
    resp = requests.post(r, files=file)
    if resp.status_code != 200:
        print("request failed!")
        return None
    return resp.content


def save_to_out_dir(content, output_dir, f):
    f = os.path.join(output_dir, f)
    f = os.path.splitext(f)[0] + ".xml"
    with open(f, "wb+") as content_file:
        content_file.write(content)


if __name__ == '__main__':
    process_sscraper('/home/mattlangsenkamp/Documents/dprl/MathSeer-extraction-pipeline/inputs/A00_3007/pdf',
                     'fake_dir')
