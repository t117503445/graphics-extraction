#####################################################################
# pipeline_test.py 
# Run the MathSeer pipeline
#
#
# Authors: Ayush Kumar Shah, Alex Keller, Richard Zanibbi  2020-2022
#####################################################################

import subprocess
import sys
import traceback
from typing import Tuple, List
import argparse
import datetime
import shutil

from multiprocessing import Pool
from moduleadapters.symbolscraper import process_sscraper

# Pipeline includes
from msp_settings import *
import msp_settings as MSP
from utils.img2LG import extract_ccs
from utils.convert_pdf_to_image import convert_pdfs, get_cc_objects
# RZ: Adding new file routines, intersection code.
import logging
import cv2
from modules.protables.ProTables import *

################################################################
# Configuration and Parameters
################################################################


def get_gpus() -> Tuple[List[str], int, int]:
    free = os.popen('nvidia-smi -q -d Memory |grep -A5 GPU|grep Free').read().strip().split('\n')
    memory_available = [int(f.split()[2]) for f in free]
    gpus = []

    # AD: Get the min memory from all gpus as the threshold
    min_mem = min(memory_available)
    for i, memory in enumerate(memory_available):
        gpus.append(str(i))

    if min_mem < 6000:
        logging.warning('WARNING!! Your GPU might not have enough VRAM '
                        'to run this pipeline')
    per_gpu_batch_size = max(1, int((min_mem - 6000) / 500))

    num_gpus = len(gpus)
    return gpus, num_gpus, per_gpu_batch_size


def parse_args():
    parser = argparse.ArgumentParser(
        description="MathSeer Pipeline: formula extraction from PDF"
    )
    parser.add_argument(
        "-i", "--input_dir",
        type=str, required=True,
        help="Directory containing input files",
    )
    parser.add_argument(
        "-o", "--output_dir",
        type=str, required=True,
        help="Directory containing output files",
    )
    parser.add_argument(
        "-l", "--log_dir",
        default=osp.join(ROOT_DIR, "logs"), type=str,
        help="Directory for logs",
    )
    parser.add_argument(
        "-c", "--convert_pdfs",
        type=int,
        default=0,
        help="Convert input pdfs to images",
    )
    parser.add_argument(
        "-v", "--visualize",
        type=int,
        default=0,
        help="Generate visualizations for detection & parsing",
    )
    parser.add_argument(
        "-b", "--batch_size",
        type=int,
        help="Batch size for ScanSSD detector",
    )
    parser.add_argument(
        "-m", "--mode",
        type=int,
        default=0,
        help="0: Math, 1: Chem",
    )
    parser.add_argument(
        "-qm", "--qdgga_model",
        type=str, default="infty_contour_lr_0.0005_Adam_los_pipeline",
        help="QD-GGA model name to be used for parsing",
    )
    parser.add_argument(
        "-le", "--last_epoch",
        type=str, default="89",
        help="QD-GGA weights (as epoch number)",
    )
    parser.add_argument(
        "-dt", "--detector",
        type=str, default="scanssd",
        choices=["yolo", "scanssd"],
        help="Which detector module to use. \"yolo\" or \"scanssd\""
    )
    parser.add_argument(
        "-pm", "--parallel_merge",
        type=int,
        default=0,
        help="Parallelize detection and PDF symbol merging",
    )

    args = parser.parse_args()

    args.gpus, num_gpus, per_gpu_batch_size = get_gpus()
    if not args.batch_size:
        args.batch_size = per_gpu_batch_size * num_gpus

    # Set additional steps.
    args.convert_pdfs = bool(args.convert_pdfs)
    args.visualize = bool(args.visualize)
    args.parallel_merge=bool(args.parallel_merge)

    # DEBUG: leave lg_output as string, to pass on to other processes.
    args.pipeline_name = args.output_dir.split("/")[-1]
    args.input_dir = osp.abspath(args.input_dir)
    if not osp.exists(args.log_dir):
        os.makedirs(args.log_dir)

    # Create output directories.
    # RZ: Only create directories that will be used.
    if args.visualize:
        os.makedirs(HTML_OUT_DIR % args.output_dir, exist_ok=True)
        os.makedirs(SCANSSD_VISUAL_DIR % args.output_dir, exist_ok=True)
        os.makedirs(TSV_IN_VIS_DIR % args.output_dir, exist_ok=True)
        os.makedirs(SSCR_VIS_DIR % args.output_dir, exist_ok=True)

    os.makedirs(SCANSSD_DETECTION_DIR % args.output_dir, exist_ok=True)
    os.makedirs(SYMBOLS_DIR % args.output_dir, exist_ok=True)
    os.makedirs(PARSE_TSV_OUT_DIR % args.output_dir, exist_ok=True)

    return args


###############################################################
# Generating Intermediate TSV files (Merging Chars, CCs)
################################################################

# Flags provided so that only CCs, or input PDF symbols can be easily seen.
def generate_tsv(args, filename, input_dir, output_dir, \
        outputCCsOnly=CC_OUTPUT_ONLY, alignCCsPDF=ALIGN_CC_SYM ):
    # Replacement symbol map; SSD path
    symbolMap = {",": "COMMA", "\"": "DQUOTE", "fraction(-)": "-"}
    
    ##############################################
    # Combine SymbolScraper + ScanSSD information
    ##############################################
    # Bounding boxes for SymbolScraper PDF characters and words
    sscraper_out_dir = osp.join(output_dir, "sscraper")
    try:  # ML: UGLY HACK if scanssd doesnt make a pro for a document for any reason this blows up
        sccraper_payload= read_sscraper_xml( sscraper_out_dir, filename, osp.join( input_dir, "images", filename) )

        (num_pages, page_sizes, pageCharBBs, pageWordBBs) = sccraper_payload
    except FileNotFoundError as e:
        logging.warning(f"could not find a png")
        logging.warning(e)
    except Exception as e:
        logging.warning(e)
        traceback.print_exc()
        logging.warning(f"could not find sscraper file: {sscraper_out_dir}/{filename}")
        return
    # Load bounding boxes from ScanSSD or YOLO
    if args.detector == 'yolo':
        region_file_path = os.path.abspath(os.path.join(YOLO_DETECTION_DIR %
               output_dir, filename) + ".csv")
    else:
        if args.mode == 0:
            region_file_path = os.path.abspath(os.path.join(SCANSSD_DETECTION_DIR %
              output_dir, "SSD", "conf_" + SCANSSD_CONF, filename) + ".csv")
        else: 
            region_file_path = os.path.abspath(os.path.join(SCANSSD_DETECTION_DIR % 
               output_dir, "SSD", "conf_" + SCANSSD_CHEM_CONF, filename) + ".csv")
    try: # ML: UGLY HACK if scanssd doesnt make a pro for a document for any reason this blows up
        pageMathBBs = read_region_csv(region_file_path, num_pages)
    except:
        print(f"could not find scannssd file: {region_file_path}")
        return
    
    # Find ScanSSD and SymbolScraper word region intersections
    # Grow regions to hold words.
    regionWordPROTable = \
        region_intersection(pageMathBBs, 'FR', pageWordBBs)
    grownRegionWordPROTable = transform_pr_table(
        grow_for_contents(page_sizes), None, regionWordPROTable)
    regionWordBBs = region_bb_list(grownRegionWordPROTable)

    # Merge characters into the regions grown around words chars are from
    regionCharPROTable = \
        region_intersection(regionWordBBs, 'FR', pageCharBBs)
    regionCharWordPROTable = \
        merge_pro_tables( grownRegionWordPROTable, regionCharPROTable  )
    
 
    ##################################
    # Get CCs for generated regions
    ##################################
    # Merge CCs, chars and words into one complete PRO table
    ccGrownProTable = get_cc_objects(input_dir, filename, regionWordBBs,
                                     "FR")
    mergedTable = merge_pro_tables(regionCharWordPROTable,
                                   ccGrownProTable)

    # Define filters for regions and objects
    # import pdb; pdb.set_trace()
    regionTests = [
        min_max_object_test( MIN_CINPUT, MAX_FINPUT, [ 'c' ] ),
        min_max_object_test( MIN_SINPUT, MAX_FINPUT, [ 'S' ] ),
        # max_height_test( 256 ), # 256 = 1in @256 dpi (MAGIC) 
        in_dictionary( [ 'en_GB', 'en_US' ], remove_alpha=True,
            report=False ),
        # max_r_area_test( max(page_sizes)[0], max(page_sizes)[1] * AREA_RATIO_LIM),
        max_height_test( HEIGHT_RATIO_LIM * max(page_sizes)[1] ), 
        max_width_test( WIDTH_RATIO_LIM * max(page_sizes)[0] ), 
    ]

    # RZ -- define filter that allows only CCs to be extracted (later ops have no effect)
    if outputCCsOnly:
        objTypeFilter = obj_keep_types( [ 'c' ] )
    else:
        objTypeFilter = obj_keep_types( [ 'c', 'S' ] )
    
    filteredPROTable = \
        filter_pr_table( combine_and_tests( regionTests), objTypeFilter, 
                mergedTable )

    # import pdb; pdb.set_trace()
    ##################################
    # Align CCs and PDF Symbols
    # where possible -- *heuristic
    ##################################
    # Parser assumes that input regions are cropped around black pixels
    primitiveModPRO = filteredPROTable
    if not outputCCsOnly and alignCCsPDF:
        transform_sym_cc = rectify_container_members( 'S', 'c', 
                threshold=INTERSECTION_THRESHOLD, crop=True ) 
        primitiveModPRO = transform_pr_table( transform_sym_cc, None,
            filteredPROTable )
    
    ##################################
    # Crop regions
    ##################################
    cropTransform = crop_contents( page_sizes )
    finalPROTable = \
        transform_pr_table(cropTransform, None, primitiveModPRO)

    ##################################
    # Write to disk as TSV
    ##################################
    # If visualizing, save TSV for words/chars from PDF in formula regions
    if args.visualize: 
        math_regions_dir = osp.join(output_dir, "sscraper")
        write_it_pro_tsv( filename, input_dir, math_regions_dir,
            ( symbolMap, regionCharWordPROTable ), page_sizes, suffix='_math_regions')

    logging.debug( pro_summary( finalPROTable, filename ) )
    parser_input_dir = osp.join(output_dir, "qdgga-parser", "input")
    write_it_pro_tsv( filename, input_dir, parser_input_dir,
            ( symbolMap, finalPROTable ), page_sizes )

'''
    Sequential TSV production (support debugging, etc.)
'''
def generate_tsv_seq(args, fileList, inputDir, outputDir ):
    for fileName in fileList:
        generate_tsv(args, fileName, inputDir, outputDir)


def genTSV_chunk(t):
    (inputDir, outputDir, fileList) = t

    for fileName in fileList:
        generate_tsv(args, fileName, inputDir, outputDir)


def parallelize_process(fn, fileList, inputDir, outputDir):
    # Partition files evenly among processes (# cores)

    # If very few files, then create each chunk with only one file
    # If fewer files than cores, create one process per file.
    n_processes = os.cpu_count()

    n = int(len(fileList) / n_processes)

    if n < 1.0:
        n = 1
        n_processes = len(fileList)

    chunks = [fileList[x:x + n] for x in range(0, len(fileList), n)]
    numChunks = len(chunks)

    # Create list of triples (inDir, outDir, file-list)
    inDirs = [inputDir] * numChunks
    outDirs = [outputDir] * numChunks
    chunkTuples = list(zip(inDirs, outDirs, chunks))

    # Create a process pool and execute with one chunk per process.
    pool = Pool(processes=n_processes)
    pool.imap(fn, chunkTuples)
    pool.close()
    pool.join()


# top-level function to combine ScanSSD and SymbolScraper output.
def combine_detections(filenames, input_dir, output_dir, parallel=False ):
    start_time = time.time()

    logging.debug("\n[ TSV Creation: Combining Visual Regions, CCs, Chars & Words ]")
    
    # Only one of the next two statements should be run!
    # DEBUGGING NOTE: In the parallel mode, error messages aren't seen
    # at the command line, as subprocess outputs2 aren't visible.
    if parallel:
        print("  [ Parallel merging ]")
        parallelize_process( genTSV_chunk, filenames, input_dir, output_dir)
    else:
        generate_tsv_seq(args, filenames, input_dir, output_dir )

    logging.debug("  TSV Generation: %.2f seconds"  % (time.time() - start_time))

################################################################
# Functions for system calls to pipeline modules
################################################################
def process_yolo(args):
    image_path = osp.join(args.input_dir, "images")
    output_path = os.path.abspath(osp.join(YOLO_DETECTION_DIR % args.output_dir))

    if args.mode == 0:
        # MATH MODE
        model = "math"
    else:
        # CHEM MODE
        model = "chem"

    os.chdir(YOLO_DIR)

    subprocess.run(["python", "docker_detection.py", "--input_dir", image_path, "--output_dir", output_path, "--model", model, "--multi_dir"] )

def process_scanssd(args):
    image_path = osp.join(args.input_dir, "images")

    # AD: Parameters for Math:0 or Chem:1
    if args.mode == 0:
        model = "trained_weights/ssd512GTDB_256_epoch15.pth"
        stride = SCANSSD_STRIDE
        conf = SCANSSD_CONF
    else:
        model = "trained_weights/ssd512GTDB_256_epoch7_chem.pth"
        stride = SCANSSD_CHEM_STRIDE
        conf = SCANSSD_CHEM_CONF

    os.chdir(SCANSSD_DIR)
    # REMOVED parameter in call:
    #   "--exp_name", filename,
    # "--post_process", "False",
    subprocess.call(
        [
            "python3", "test.py",
            "--dataset_root", args.input_dir,
            "--trained_model", model,
            "--cuda", "True",
            "--save_folder", osp.join(SCANSSD_DETECTION_DIR % args.output_dir),
            "--log_dir", osp.join(ROOT_DIR, "logs"),
            "--test_data", osp.join(args.input_dir, "pdf_list"),
            "--model_type", "512",
            "--cfg", "math_gtdb_512",
            "--padding", "0", "2",
            "--kernel", "1", "5",
            "--batch_size", str(args.batch_size),
            "--stride", stride,
            "--op_mode", "pipeline",
            "--gpu", *args.gpus,
            "--post_process", "0",
            "--conf", conf,
            "--window", "512"
        ]
    )
    os.chdir(SRC_DIR)


def visualize_detection(args, out_dir_name, out_dir_conf):
    image_path = osp.join(args.input_dir, "images")
    
    # Change here because this is where the visualize annotations script lives.
    os.chdir(SCANSSD_DIR)
    if args.detector == 'yolo':
        subprocess.call(
            [
                "python3", "visualize_annotations.py",
                "--img_dir", image_path,
                "--out_dir", osp.join(YOLO_VISUAL_DIR % args.output_dir),
                "--math_dir", osp.join(YOLO_DETECTION_DIR % args.output_dir + "/")
            ]
        )

        pass
    else:
        subprocess.call(
            [
                "python3", "visualize_annotations.py",
                "--img_dir", image_path,
                "--out_dir", osp.join(SCANSSD_VISUAL_DIR % args.output_dir),
                "--math_dir", osp.join(SCANSSD_DETECTION_DIR % args.output_dir,
                                       out_dir_name, "conf_" + str(out_dir_conf) + "/")
            ]
        )

    os.chdir(SRC_DIR)


def new_vis_tsv(args, indir, outdir, suffix=''):

    image_path = osp.join(args.input_dir, "images")
    os.chdir(SCANSSD_DIR)

    subprocess.call(

        [
            "python3", "visualize_annotations.py",
            "--img_dir", image_path,

            "--out_dir", outdir, 
            "--tsv_dir", indir,
            "--suffix", suffix,
        ]
    )
    os.chdir(SRC_DIR)


def run_parser(args, output_dir, model_name, last_epoch):
    # RZ: Added parameter 'vis' to optionally turn off dot/pdf conversion
    start = time.time()

    tsv_in_dir = os.path.join(PARSE_TSV_INPUT_DIR % output_dir)

    # Page images
    image_path = os.path.join(args.input_dir, "images")

    # Create TSV output dir + visualization (if requested) directories
    tsv_out_dir = osp.join(PARSE_TSV_OUT_DIR % output_dir)
    os.makedirs(tsv_out_dir, exist_ok=True)
    dot_out_dir = ''
    if args.visualize:
        dot_out_dir = os.path.abspath(PARSE_DOT_OUT_DIR % output_dir)
        os.makedirs(dot_out_dir, exist_ok=True)

    # Mesages
    logging.debug("\n  Configuration: " + osp.join(QDGGA_DIR,
                                                   "config/infty.yaml"))
    logging.debug("  Output TSV dir: {}".format(os.path.abspath(
        tsv_out_dir)))
    logging.debug("  Input TSV dir: {}".format(os.path.abspath(
        tsv_in_dir)))

    # Invoke the QD-GGA parser
    os.chdir(QDGGA_DIR)

    print("  * Inputs (TSV): TSV + page images")
    subprocess.call(
        [
            "python3", "test.py",
            "--config", osp.join(QDGGA_DIR, "config/defaultINFTY.yaml"),
            "--run_config", osp.join(QDGGA_DIR, "config/run.yaml"),
            "--data_dir", image_path,
            "--tsv_dir", tsv_in_dir,
            "--optional", "1",
            "--given_sym", "1",
            "--model_dir", "outputs/run",
            "--model_name", model_name,
            "--output_dir", tsv_out_dir,
            "--last_epoch", last_epoch,
            "--dot_dir", dot_out_dir,
            "--visualize", str(int(args.visualize)),
        ]
    )

    end = time.time()
    logging.debug("  Parse time: {:.2f}".format(end - start) \
                  + " seconds")

    os.chdir(SRC_DIR)

def write_yolo_det_meta_info(args):
    file_loc_det = osp.join(YOLO_DETECTION_DIR % args.output_dir, "_YOLOv4")
    file_loc_vis = osp.join(YOLO_VISUAL_DIR % args.output_dir, "_YOLOv4")

    with open(file_loc_det, "w") as f:
        f.write(f"YOLO detection done with: \nimgSize = {YOLO_IMG_SIZE}\nConf = {YOLO_CONF}\nNMS IOU Threshold = {YOLO_NMS_IOU}\n")

    with open(file_loc_vis, "w") as f:
        f.write(f"YOLO detection done with: \nimgSize = {YOLO_IMG_SIZE}\nConf = {YOLO_CONF}\nNMS IOU Threshold = {YOLO_NMS_IOU}\n")


################################################################
# Main Program
################################################################
if __name__ == "__main__":

    start = time.time()
    args = parse_args()
    print("")
    print("--------------------------------------------------------")
    print("  Graphics Extraction Pipeline")
    print("  Document and Pattern Recognition Lab, RIT")
    print("  v0.1.3, June 2022")
    print("")#osp.join(PARSE_TSV_INPUT_DIR % args.output_dir )
    print("  Please see README.md for a list of contributors.")

    print("--------------------------------------------------------")
    print("")

    now = datetime.datetime.now()
    dt_string = now.strftime("Date (D/M/Y): %d/%m/%Y   Time: %H:%M:%S")
    print(dt_string)
    print("Visualizations: " + str(args.visualize))

    # Collecting directory definitions
    TSV_VIS_DIR = osp.join(TSV_IN_VIS_DIR % args.output_dir)
    TSV_PARSEIN_DIR = osp.join( PARSE_TSV_INPUT_DIR % args.output_dir )
    PDF_VIS_DIR = osp.join( SSCR_VIS_DIR % args.output_dir)
    SYMBOLS_IN_DIR = osp.join( SYMBOLS_DIR % args.output_dir )

    try:
        ################################################################
        # Initialization / Setup / Page Image Generation
        ################################################################
        # Initiate logging and handle command line arguments.
        filepath = os.path.join(args.log_dir, args.pipeline_name + "_" +
                                str(round(time.time())) + ".log")
        print("Logging pipeline to:\n  " + osp.abspath(filepath))
        logging.basicConfig(
            filename=filepath, filemode="w",
            format="%(process)d - %(asctime)s - %(message)s",
            datefmt="%d-%b-%y %H:%M:%S", level=logging.DEBUG,
        )
        consoleHandler = logging.StreamHandler(sys.stdout)
        logging.getLogger().addHandler(consoleHandler)

        logging.debug("\n[ Command-Line Arguments ]")
        logging.debug(str(args).replace('(', '(\n  ').replace(',', ',\n  '))

        with open(osp.join(args.input_dir, "pdf_list"), "r") as file:
            filenames = [line.strip() \
                         for line in file if line and line[0] != "#"]

            # Convert images if asked.
        if args.convert_pdfs:
            convert_pdfs(args.input_dir)

        logging.debug("\n**** Initialization time: {:.2f}".format(
            time.time() - start) + " seconds ****")

        ################################################################
        # Recognition Tasks
        ################################################################

        rec_start = time.time()

        # 1. Generate parse inputs (from region, PDF symbols) as TSVs
        if not PIPE_SKIP_PREPARSE and not PIPE_PARSE_ONLY:
            if args.detector == 'yolo':
                process_yolo(args)
            else:
                process_scanssd(args)
            process_sscraper(osp.join(args.input_dir, "pdf"), SYMBOLS_DIR % args.output_dir)
            if PIPE_COMBINE_IF_PREPARSE:
                combine_detections(filenames, args.input_dir, args.output_dir,
                        args.parallel_merge)

        # RZ: HACK
        if PIPE_COMBINE_ONLY and not PIPE_PARSE_ONLY:
            combine_detections(filenames, args.input_dir, args.output_dir,
                    args.parallel_merge)
            new_vis_tsv(args, SYMBOLS_IN_DIR, PDF_VIS_DIR, '_math_regions' )
            new_vis_tsv(args, TSV_PARSEIN_DIR, TSV_VIS_DIR )
            sys.exit(0)

        # 2. Parse formulas, create output TSVs ('-out')
        if not PIPE_SKIP_PARSE:
            pstart = time.time()
            logging.debug("\n[ QD-GGA Parser ]")
            run_parser(args, args.output_dir, args.qdgga_model,
                       args.last_epoch)

            pend = time.time()
            logging.debug("\n  Total parse time: {:.2f}" \
                          .format(pend - pstart) + " seconds")

        logging.debug("\n****  Recognition Time: {:.2f}" \
                      .format(time.time() - rec_start) + " seconds ****")

        ################################################################
        # Visualization Tasks
        ################################################################
        # RZ: Visualization moved to end of processing chain
        if args.visualize:

            vstart= time.time()

            print(args)
            # Visualize ScanSSD, ScanSSD + SymbolScraper,
            # Full QD-GGA input data
            if args.detector == 'yolo':
                if args.mode == 0:
                    visualize_detection(args, "yolo", YOLO_CONF)
                else:
                    visualize_detection(args, "yolo", YOLO_CONF)
            else:
                if args.mode == 0:
                    visualize_detection(args, "SSD", SCANSSD_CONF)
                else:
                    visualize_detection(args, "SSD", SCANSSD_CHEM_CONF)
            new_vis_tsv(args, SYMBOLS_IN_DIR, PDF_VIS_DIR, '_math_regions' )
            new_vis_tsv(args, TSV_PARSEIN_DIR, TSV_VIS_DIR )

            # Generate HTML summary pages.
            print(filenames)
            create_htmls(filenames, args.input_dir, args.output_dir,
                         args.qdgga_model, args.last_epoch, MSP)

            logging.debug("\n**** Visualization Time {:.2f}". \
                          format(time.time() - vstart) + " seconds ****")

            if args.detector == 'yolo':
                write_yolo_det_meta_info(args)


    except Exception as e:
        logging.error("Exception creating log occurred", exc_info=True)
        print("Exception creating log: \n{}".format(e))

    # Wrap up.
    end = time.time()
    logging.debug("\n[[[  Pipeline Finished  ]]]")
    logging.debug("  Total time: {:.2f}".format(end - start) + " seconds")
