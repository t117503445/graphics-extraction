import os


def qdgga_service(server, namespace, i, exposed_port) -> str:
    return qdgga_service_template.format(i=i, namespace=namespace, server=server, exposed_port=exposed_port)

def create_and_save_qdgga_services(servers, namespace, spec_dir):

    os.makedirs(spec_dir, exist_ok=True)
    services = []
    for i, s in enumerate(servers):
        key = ''
        assert len(s.keys()) > 0
        for k in s.keys():
            key = k
        
        service_file = os.path.join(spec_dir, f"{key}_qdgga_{namespace}.yml")
        services.append(f"qdgga-{i}")
        service_spec = qdgga_service(key, namespace, i, s[key]['ports'])
        with open(service_file, 'w+') as f:
            f.write(service_spec)


qdgga_service_template = """apiVersion: v1
kind: Service
metadata:
  name: qdgga-{i}
  namespace: {namespace}
spec:
  selector:
    app: qdgga-{i}
  type: NodePort
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
    nodePort: {exposed_port}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: qdgga-{i}
  namespace: {namespace}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: qdgga-{i}
  template:
    metadata:
      labels:
        app: qdgga-{i}
    spec:
      nodeName: {server}
      containers:
      - name: qdgga-{i}
        image: "dprl/qdgga:latest"
        imagePullPolicy: Always
        ports:
        - containerPort: 80
"""
