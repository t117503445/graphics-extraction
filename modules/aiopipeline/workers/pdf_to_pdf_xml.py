import multiprocessing
from io import BytesIO
from queue import Empty
from typing import List, Dict
from multiprocessing import Queue, Event
import aiohttp
from copy import copy
import logging
from modules.aiopipeline.model.LoadedPdf import LoadedPdf
import os
import asyncio
from modules.aiopipeline.workers.utils import check_status

logger = logging.getLogger("pdf_to_pdf_xml")


def pdf_to_pdf_xml(loaded_queue: Queue,
                   pdf_xml_queue: Queue,
                   loader_done: Event,
                   sscraper_done: Event,
                   config: Dict,
                   server_i: int):
    host = 'localhost'
    cur_server_conf = config['servers']['sscraper'][server_i]
    cur_server_name = list(cur_server_conf.keys())[0]
    sscraper_port = cur_server_conf[cur_server_name]['ports']
    loop = asyncio.get_event_loop()
    while not (loader_done.is_set() and loaded_queue.empty()):
        try:
            cur_pdfs: List[LoadedPdf] = loaded_queue.get(timeout=1)
            next_pdf_xmls = loop.run_until_complete(send_pdfs_to_sscraper(cur_pdfs, host, sscraper_port))
            pdf_xml_queue.put(next_pdf_xmls)
        except Empty:
            pass
        except Exception as e:
            logger.warning(e)

    sscraper_done.set()
    logger.info("sscraper done")


async def send_pdfs_to_sscraper(pdfs: List[LoadedPdf], host, port):
    session = aiohttp.ClientSession()
    coroutines = []
    for pdf in pdfs:
        logger.info(f"sending {pdf.file_name} to sscraper for processing")
        url = f'http://{host}:{port}/process-pdf?writePageBorderBBOX=true'
        file = {"pdf": copy(pdf.pdf)}
        coroutines.append(session.post(url, data=file))
    responses = await asyncio.gather(*coroutines)

    pdf_xmls = []
    for resp, pdf in zip(responses, pdfs):
        await check_status(resp, logger)
        content = await resp.content.read()
        pdf_xmls.append(pdf.to_pdf_xml(BytesIO(content)))
    await session.close()
    return pdf_xmls
