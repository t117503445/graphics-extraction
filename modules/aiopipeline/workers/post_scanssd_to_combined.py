import io
import shutil
from queue import Empty
from pdf2image import convert_from_bytes
from typing import List, Dict
from multiprocessing import Queue, Event
from modules.aiopipeline.workers.utils import all_true
from modules.pipeline.utils.convert_pdf_to_image import get_cc_objects_from_img_list
from modules.protables.ProTables import *
from modules.aiopipeline.model.Combined import Combined
from modules.aiopipeline.model.PostScanSSD import PostScanSSD
from copy import copy
from PIL.Image import Image
import uuid
import logging
import modules.pipeline.msp_settings as MSP

logger = logging.getLogger('combining')


def combine_scanssd_sscraper(post_scanssd_queue: Queue,
                             combined_queue: Queue,
                             scanssd_done: List[Event],
                             combined_done: Event,
                             out_dir,
                             in_dir, 
                             config: Dict):
    while not (all_true(scanssd_done) and post_scanssd_queue.empty()):
        try:
            cur_combined: List[PostScanSSD] = post_scanssd_queue.get(timeout=1)
            combined_batch: List[Combined] = combine_all_csv_and_xml(cur_combined, out_dir, in_dir, config)
            combined_queue.put(combined_batch)
        except Empty:
            pass
        except Exception as e:
            logger.warning(e)

    logger.info("combined worker done")
    combined_done.set()


def combine_all_csv_and_xml(cur_post_scanssd: List[PostScanSSD], out_dir, in_dir, config: Dict) -> List[Combined]:
    return [combine_csv_and_xml(p_ssd, out_dir, in_dir, config) for p_ssd in cur_post_scanssd]


def combine_csv_and_xml(cur_post_scanssd: PostScanSSD, out_dir, in_dir, config: Dict) -> Combined:
    logger.info(f"combining scanssd and sscraper data into a protable for {cur_post_scanssd.file_name}")
    vis = config['parameters']['vis']
    folder, img_output_folder = create_temp_dirs()
    file_name_no_ext = os.path.basename(cur_post_scanssd.file_name)
    img_list = load_images(cur_post_scanssd, img_output_folder, vis, in_dir, out_dir)

    png_height, png_width = img_list[0].height, img_list[0].width

    logger.info("other stuff")
    num_pages, page_char_bbs, page_word_bbs = load_sscraper_data(
        cur_post_scanssd, file_name_no_ext, png_width, png_height)

    f_page_math_bbs = load_scanssd_data(cur_post_scanssd, num_pages)

    region_char_word_pro_table, region_word_bbs = get_math_regions(
        f_page_math_bbs, page_word_bbs, png_width, png_height, page_char_bbs)

    print("writing math regions")
    symbol_map = {",": "COMMA", "\"": "DQUOTE", "fraction(-)": "-"}
    tsv_region_bytes = write_math_regions(file_name_no_ext, folder, symbol_map, region_char_word_pro_table)

    print("getting connected components and merging")
    merged_table = get_cc_and_merge(img_list, region_word_bbs, region_char_word_pro_table)

    final_pro_table = get_final_pro(merged_table, png_width, png_height)

    write_it_pro_tsv("final_pro", "examples", folder,
                     (symbol_map, final_pro_table))

    tsv_f = open(osp.join(folder, "final_pro.tsv"), 'rb')
    raw_bytes = tsv_f.read()
    tsv_bytes = copy(io.BytesIO(copy(raw_bytes)))
    tsv_f.close()
    logger.info(f"combined scanssd and sscraper data into a protable for {cur_post_scanssd.file_name}")
    shutil.rmtree(folder)

    combined = cur_post_scanssd.to_combined(tsv=tsv_bytes, region_tsv=tsv_region_bytes)

    if vis:
        combined.save_xml(os.path.join(MSP.ROOT_DIR, out_dir, "qdgga", "input"))

    return combined


def create_temp_dirs():
    folder_uuid = str(uuid.uuid4())
    folder = osp.join("/", "tmp", folder_uuid)
    os.makedirs(folder, exist_ok=True)
    img_output_folder = osp.join(folder, "images")
    os.makedirs(img_output_folder, exist_ok=True)
    return folder, img_output_folder


def load_images(cur_post_scanssd, img_output_folder, vis, in_dir, out_dir):
    img_list: List[Image] = convert_from_bytes(
        copy(cur_post_scanssd.pdf).read(),
        dpi=256,
        output_folder=img_output_folder)
    if vis:
        file_name = os.path.splitext(cur_post_scanssd.file_name)[0]
        logger.info(f"saving images for {file_name}")
        os.makedirs(osp.join(MSP.ROOT_DIR, in_dir, 'images', file_name), exist_ok=True)
        os.makedirs(osp.join(MSP.ROOT_DIR, out_dir, 'view', 'qdgga', 'input', file_name), exist_ok=True)
        for i, img in enumerate(img_list):
            img.save(osp.join(MSP.ROOT_DIR, in_dir, 'images', file_name, f'{i}.png'), format="png")
            img.save(osp.join(MSP.ROOT_DIR, out_dir, 'view', 'qdgga', 'input', file_name, f'{i}.png'), format="png")

    return img_list


def load_sscraper_data(cur_post_scanssd, file_name_no_ext, png_width, png_height):
    sscraper_string_io = io.TextIOWrapper(copy(cur_post_scanssd.xml), encoding='utf-8')
    num_pages, page_char_bbs, page_word_bbs = read_sscraper_xml_from_io(
        sscraper_string_io, file_name_no_ext, png_width, png_height)
    return num_pages, page_char_bbs, page_word_bbs


def load_scanssd_data(cur_post_scanssd, num_pages):
    csv_string_io = io.TextIOWrapper(copy(cur_post_scanssd.csv), encoding='utf-8')
    page_math_bbs = read_region_csv_from_io(csv_string_io, num_pages)
    return page_math_bbs


def get_math_regions(f_page_math_bbs, page_word_bbs, png_width, png_height, page_char_bbs):
    region_word_pro_table = region_intersection(f_page_math_bbs, 'FR', page_word_bbs)
    grown_region_word_pro_table = transform_pr_table(
        grow_for_contents(png_width, png_height),
        None, region_word_pro_table)
    region_word_bbs = region_bb_list(grown_region_word_pro_table)

    region_char_pro_table = region_intersection(region_word_bbs, 'FR', page_char_bbs)
    region_char_word_pro_table = merge_pro_tables(grown_region_word_pro_table, region_char_pro_table)
    return region_char_word_pro_table, region_word_bbs


def write_math_regions(file_name_no_ext, folder, symbol_map, region_char_word_pro_table):
    write_it_pro_tsv(file_name_no_ext, "examples", folder,
                     (symbol_map, region_char_word_pro_table), suffix='_math_regions')
    tsv_region_f = open(osp.join(folder, f"{file_name_no_ext}_math_regions.tsv"), 'rb')
    raw_region_bytes = tsv_region_f.read()
    tsv_region_bytes = io.BytesIO(copy(raw_region_bytes))
    tsv_region_f.close()
    return tsv_region_bytes


def get_cc_and_merge(img_list, region_word_bbs, region_char_word_pro_table):
    cc_grown_pro_table = get_cc_objects_from_img_list(img_list, region_word_bbs, "FR")

    merged_table = merge_pro_tables(region_char_word_pro_table,
                                    cc_grown_pro_table)
    return merged_table


def get_final_pro(merged_table, png_width, png_height):
    region_tests = [
        min_max_object_test(2, 60, ['c', 'S']),
        max_height_test(256),  # 256 = 1in @256 dpi (MAGIC)
        in_dictionary(['en_GB', 'en_US'], remove_alpha=True,
                      report=False)
    ]
    obj_type_filter = obj_keep_types(['c', 'S'])  # , 'S'
    filtered_pro_table = filter_pr_table(combine_and_tests(region_tests), obj_type_filter,
                                         merged_table)
    transform_sym_cc = rectify_container_members('S', 'c',
                                                 threshold=INTERSECTION_THRESHOLD, crop=True)
    primitive_mod_pro = transform_pr_table(transform_sym_cc, None,
                                           filtered_pro_table)
    crop_transform = crop_contents(png_width, png_height)
    final_pro_table = transform_pr_table(crop_transform, None, primitive_mod_pro)
    return final_pro_table
