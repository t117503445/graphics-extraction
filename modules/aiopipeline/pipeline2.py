from copy import copy
from typing import List
import yaml
from modules.aiopipeline.model.Combined import Combined
from modules.aiopipeline.model.LoadedPdf import LoadedPdf
from modules.aiopipeline.model.PdfXml import PdfXml
import argparse
from modules.aiopipeline.model.PostParser import PostParser
from modules.aiopipeline.model.PostScanSSD import PostScanSSD
from modules.aiopipeline.workers.pdf_xml_to_post_scanssd import pdf_xml_to_post_scanssd
from modules.aiopipeline.workers.pdf_to_pdf_xml import pdf_to_pdf_xml
from modules.aiopipeline.workers.pdf_loader import pdf_loader
from modules.aiopipeline.workers.post_scanssd_to_combined import combine_scanssd_sscraper
from modules.aiopipeline.workers.combined_to_post_parser import combined_to_post_parser
from modules.aiopipeline.workers.save_and_generate_html import save_and_generate_html
from multiprocessing import Queue, Process, Event
from dotenv import load_dotenv
from pathlib import Path
import os
import time
from os.path import exists
import sys
import logging
import modules.pipeline.msp_settings as MSP
from modules.protables.ProTables import create_htmls
from modules.protables.createHTML import *
from modules.pipeline.pipeline_test import visualize_detection, new_vis_tsv
from modules.utils.easydict import EasyDict

logger = logging.getLogger("main")


def main(args):
    with open('aio-config.yml', 'r') as file:
        config = yaml.safe_load(file)
    mini_batch_size = config['parameters']['mini_batch_size']
    # configure logging
    os.makedirs(os.path.join('logs', 'aiopipeline'), exist_ok=True)
    log_name = f'{os.path.split(args.input_dir)[1]}--{time.strftime("%d-%m-%Y-%H:%M:%S", time.gmtime())}.log'
    logging.basicConfig(
        format='%(levelname)-9s:%(asctime)s:\t%(name)-28s:%(message)s',
        filename=f'logs/aiopipeline/{log_name}',
        level=logging.DEBUG)
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

    # make PIL be quiet
    pil_logger = logging.getLogger('PIL')
    pil_logger.setLevel(logging.INFO)

    asyncio_logger = logging.getLogger('asyncio')
    asyncio_logger.setLevel(logging.WARNING)

    start_time = time.monotonic()

    loaded_queue: Queue[List[LoadedPdf]] = Queue()
    pdf_xml_queue: Queue[List[PdfXml]] = Queue()
    post_scanssd_queue: Queue[List[PostScanSSD]] = Queue()
    combined_queue: Queue[List[Combined]] = Queue()
    post_parser_queue: Queue[List[PostParser]] = Queue()

    input_dir = args.input_dir
    pdf_list_loc = os.path.join(input_dir, 'pdf_list')
    if exists(pdf_list_loc):
        pdf_list = open(pdf_list_loc, 'r')
        pdf_files = pdf_list.readlines()
    else:
        logger.error("malformed input directory. unable to find 'pdf_list' file. pipeline cannot continue.")
        sys.exit(1)

    # completion create events
    loader_done = Event()
    sscraper_done = [Event() for _ in range(len(config['servers']['sscraper']))]
    scanssd_done = [Event() for _ in range(len(config['servers']['scanssd']))]
    combine_done = [Event() for _ in range(2)]
    parser_done = [Event() for _ in range(len(config['servers']['qdgga']))]


    file_names = copy(pdf_files)
    # make loader
    loader = Process(target=pdf_loader, args=(input_dir, pdf_files, loaded_queue, mini_batch_size, loader_done))
    # attach sscraper worker to loader
    sscraper = []
    for i in range(len(sscraper_done)):
        sscraper.append(Process(
            target=pdf_to_pdf_xml, 
            args=(loaded_queue, pdf_xml_queue, loader_done, sscraper_done[i], config, i)
        ))
    # attach scanssd worker to sscraper
    scanssd = []
    for i in range(len(scanssd_done)):
        scanssd.append(Process(
            target=pdf_xml_to_post_scanssd,
            args=(pdf_xml_queue, post_scanssd_queue, sscraper_done, scanssd_done[i], config, i)
        ))
    # attach scanssd and sscraper data to combiner process
    combined = []
    for i in range(len(combine_done)):
        combined.append(Process(
            target=combine_scanssd_sscraper,
            args=(post_scanssd_queue, combined_queue, scanssd_done, combine_done[i], args.output_dir, args.input_dir, config)
        ))
    # attach the combiner queue to the function that makes parser requests
    parser2 = []
    for i in range(len(parser_done)):
        parser2.append(Process(
            target=combined_to_post_parser,
            args=(combined_queue, post_parser_queue, combine_done, parser_done[i], config, i)
        ))

    html = Process(
        target=save_and_generate_html,
        args=(post_parser_queue, parser_done, args.output_dir, args.input_dir)
    )


    loader.start()
    for s in sscraper:
        s.start()
    for s in scanssd:
        s.start()
    for c in combined:
        c.start()
    for p in parser2:
        p.start()

    html.start()
    logger.info("started all processes")

    loader_done.wait()
    end_time = time.monotonic()
    time.sleep(0.1)
    logging.info(f"time taken for loader {end_time-start_time}")
    loader.join()
    
    for s in sscraper_done:
        s.wait()
    end_time = time.monotonic()
    time.sleep(0.1)
    logging.info(f"time taken for sscraper {end_time-start_time}")
    for s in sscraper:
        s.join()

    for s in scanssd_done:
        s.wait()
    end_time = time.monotonic()
    time.sleep(0.1)
    logging.info(f"time taken for scanssd {end_time-start_time}")
    for s in scanssd:
        s.join()

    for c in combine_done:
        c.wait()
    end_time = time.monotonic()
    time.sleep(0.1)
    logging.info(f"time taken to combine xml and csv {end_time-start_time}")
    for c in combined:
        c.join()

    for p in parser_done:
        p.wait()
    end_time = time.monotonic()
    time.sleep(0.1)
    logging.info(f"time taken to parse documents {end_time-start_time}")
    end_time = time.monotonic()
    for p in parser2:
        p.join()


    html.join()
    logging.info("files written")
    logging.info(f"total pipeline time {end_time-start_time}")

    vis = bool(config['parameters']['vis'])
    if vis:
        conf = float(config['parameters']['conf'])
        args_loc = EasyDict({
            "output_dir": args.output_dir,
            "input_dir": osp.join(MSP.ROOT_DIR, args.input_dir),
            "qdgga_model": "infty_contour",
            "last_epoch": "89"})
        visualize_detection(args_loc, "SSD", conf)

        SYMBOLS_IN_DIR = osp.join(MSP.ROOT_DIR, args.output_dir, 'sscraper')
        PDF_VIS_DIR = osp.join(MSP.ROOT_DIR, args.output_dir, 'view', 'sscraper')
        TSV_PARSEIN_DIR = osp.join(MSP.ROOT_DIR, args.output_dir, 'qdgga', 'input')
        TSV_VIS_DIR = osp.join(MSP.ROOT_DIR, args.output_dir, 'view', 'qdgga', 'input')

        
        new_vis_tsv(args_loc, SYMBOLS_IN_DIR, PDF_VIS_DIR, '_math_regions' )
        new_vis_tsv(args_loc, TSV_PARSEIN_DIR, TSV_VIS_DIR )
            # Generate HTML summary pages.
        file_names = [f.strip("\n") for f in file_names]

        create_htmls(file_names, args_loc.input_dir, args_loc.output_dir,
                     args_loc.qdgga_model, args_loc.last_epoch, MSP)


if __name__ == "__main__":

    arg_parser = argparse.ArgumentParser(description='Run the distributed asynchronous mathseer pipeline')
    arg_parser.add_argument('-i', '--input_dir', help="the input directory for the collection to process", type=str)
    arg_parser.add_argument('-o', '--output_dir', help="the output directory for the collection to process", type=str)
    arg_parser.add_argument('-d', '--debug', action='store_true', help="add this flag to save intermediate values from "
                                                                   "each step in the pipeline")
    arg_parser.add_argument('-v', '--visualize', action='store_true', help="add this flag to visualize the results")
    args = arg_parser.parse_args()
    main(args)

