import os.path

import aiohttp
import asyncio
import argparse
import time
import requests


async def aiohttp1(pdf_files, endpoint):
    async with aiohttp.ClientSession() as session:
        coroutines = []
        for pdf in pdf_files:
            file = {"pdf": open(pdf, 'rb')}
            coroutines.append(session.post(endpoint, data=file))
        responses = await asyncio.gather(*coroutines)
        print(responses)


def sync_calls(pdf_files, endpoint):

    for pdf in pdf_files:
        filename = os.path.split(pdf)[0]
        file = {"pdf": (filename, open(pdf, 'rb'), "application/pdf")}
        resp = requests.post(endpoint, files=file)
        if resp.status_code != 200:
            print("request failed!")


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--endpoint', default="http://localhost:7002/process-pdf")
    args = parser.parse_args()

    files = ['inputs/ACL/pdf/A00-3007.pdf',
             'inputs/ACL/pdf/K15-1002.pdf',
             'inputs/ACL/pdf/K15-1004.pdf',
             'inputs/ACL/pdf/W07-0901.pdf',
             'inputs/ACL/pdf/W07-1428.pdf']
    start = time.time()
    asyncio.run(aiohttp1(files, args.endpoint))
    end = time.time()
    print(f"time taken to process pdfs async {str(end - start)}")

    start = time.time()
    sync_calls(files, args.endpoint)
    end = time.time()
    print(f"time taken to process pdfs sync {str(end - start)}")

